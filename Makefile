##
## Makefile
## Rules to create .deb packages for local-installed Go.
##

SHELL	:= /bin/sh

# WARNING: don't add an ending / at the URL, download will fail!
BASE_URL	:= https://go.dev/dl

# Pick the latest version by default
# User can override it by supplying GOVER=??? at cmdline
GOVER		:= $(shell sed 's/^go\([0-9.]*\)\..*/\1/' versions | sort -V | tail -1)
ARCH		:= amd64

all: build


##
## Dependencies/Rules
## This part of the Makefile implements its functionality.
## No need to confuse the user with extra details, not needed for customization.
## All necessary information that a user can touch can be found in the
## beginning of the file.
##

SOURCE		 = files/go${GOVER}.linux-${ARCH}.tar.gz

help:
	@echo 'Create a Debian package for local-installed Go.'
	@echo
	@echo 'The local Go environment is installed at /usr/local/share/.'
	@echo 'Binaries exist in the built-default locations: /usr/local/bin/.'
	@echo 'By default, build the latest available Go version.'
	@echo 'Set the $$GOVER variable to build a different version:'
	@echo '   $$ make GOVER=1.3.0'

build: clean ${SOURCE}
	@echo "creating Debian package for ${GOVER}"
	scripts/check_file versions ${SOURCE}
	scripts/build-pkg ${GOVER}
update: sources
sources: versions.yml
veryclean: cleanall
cleanall: clean
	@echo 'removing packages & cached files'
	$(RM) *.deb files/*
clean:
	@echo 'removing build directory'
	$(RM) -r BUILD
	@echo 'removing other created files'
	$(RM) DEBIAN/md5sums

${SOURCE}: sources
	mkdir -p files
	wget -P files/ ${BASE_URL}/${@F}

versions.yml: versions
	@echo 'creating the version'
	echo 'base_url:   ${BASE_URL}' > $@
	scripts/gen-versions $^ >> $@

.PHONY: all build help deb clean cleanall veryclean sources update
